<?php

namespace App\Contracts\Dao\Customer;
use App\Models\Customer;

interface CustomerDaoInterface
{
    public function validateEmail($customeremail);
    public function insertRecord($customerid,$customername,$customeremail,$customerpassword,$authority);
    public function autoId($tableName,$fieldName,$prefix,$noOfLeadingZeros);
    public function loginAction($id,$pass);

    public function searchMenu($customerId, $customerName, $authority, $email, $startDate, $endDate);
    public function edit($customerId);
    public function  store($customerId,$customerName,$authority,$email);
    public function destroy($customerId);
}
