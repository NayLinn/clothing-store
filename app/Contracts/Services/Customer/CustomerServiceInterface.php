<?php

namespace App\Contracts\Services\Customer;

interface CustomerServiceInterface
{
    public function validateEmail($customeremail);
    public function insertRecord($customerid,$customername,$customeremail,$customerpassword,$authority);
    public function autoId($tableName,$fieldName,$prefix,$noOfLeadingZeros);
    public function loginAction($id,$pass);

    public function search($customerId, $customerName,$authority,$email,$startDate,$endDate);
    public function edit($customerId);
    public function store($customerId,$customerName,$authority,$email);
    public function destroy($customerId);
}
