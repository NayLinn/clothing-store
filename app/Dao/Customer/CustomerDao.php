<?php

namespace App\Dao\Customer;
use Hash;
use DB;
use App\Contracts\Dao\Customer\CustomerDaoInterface;
use App\Models\Customer;

/****************************************************************************************************
 * システムＩＤ　：
 * システム名称　：ほぐしWEB予約システム
 * モジュール名称：顧客情報DAO
 * 改訂履歴　　　：v1.0
 * Copyright(C) 2014-2016, CIN Group Inc, All Rights Reserved.
 ****************************************************************************************************/
class CustomerDao implements CustomerDaoInterface
{
    public function validateEmail($customeremail){
        //sql query using customer model
        $customers = Customer::where('email',$customeremail)->first();
        return $customers;
    }

    public function insertRecord($customerid,$customername,$customeremail,$customerpassword,$authority){
        //sql qurey using DB
        DB::table('customer')->insert(
          array('customer_id'=>$customerid,'customer_name' => $customername, 'email' => $customeremail,'password' => $customerpassword,'authority' => $authority,'register_datetime'=>date("Y-m-d H:i:s"),'update_datetime'=>date("Y-m-d H:i:s"))
        );

        $row=DB::table('customer')->select('customer_id')
                                         ->where('customer_id',$customerid)
                                         ->first();
        if($row){
          $id=$row->customer_id;
          return $id;
        }
        else{
          return null;
        }
    }

    public function autoId($tableName,$fieldName,$prefix,$noOfLeadingZeros){
      $row=DB::table($tableName)->select($fieldName)->orderBy('register_datetime','desc')->first();
      if($row){
      $id=$row->customer_id;
      return $id;
      }
      else{
        return null;
      }
    }

    public function loginAction($id,$pass){
      $row = Customer::where('customer_id',$id)->get();
      if($row){
        return $row;
      }
      else{
        return null;
      }
    }


    //////////////////////////////////////


    public function searchMenu($customerId, $customerName, $authority, $email, $startDate, $endDate)
    {
        if ($customerId == null && $customerName == null && $authority == null && $email == null && $startDate == null && $endDate == null) {
            $custLists = null;
        } else {
            if (isset($startDate) && !empty($startDate) && !is_null($startDate)) {
                $query = DB::table('customer')
                    ->whereBetween('register_datetime', array($startDate, $endDate));

            } else {
                $query = DB::table('customer')->select('customer_id', 'customer_name', 'authority', 'email', 'register_datetime', 'update_datetime');
            }

            if (!is_null($customerId)) {
                $query->where('customer_id', 'LIKE', '%' . $customerId . '%');
            }

            if (!is_null($customerName)) {
                $query->where('customer_name', 'LIKE', '%' . $customerName . '%');
            }
            if (!is_null($authority)) {
                $query->where('authority', 'LIKE', '%' . $authority . '%');
            }

            if (!is_null($email)) {
                $query->where('email', 'LIKE', '%' . $email . '%');
            }
            $custLists = $query->paginate(3);
            return $custLists;
        }

    }

    public function edit($customerId)
    {
        $editList = DB::table('customer')
            ->where('customer_id', $customerId)
            ->first();
        return $editList;
    }

    public function store($customerId, $customerName, $authority, $email)
    {
        $editLists = DB::table('customer')
            ->where('customer_id', $customerId)
            ->update([
                'customer_name' => $customerName,
                'authority'     => $authority,
                'email'         => $email,
            ]);
        return $editLists;
    }

    public function destroy($customerId)
    {
        return Customer::where('customer_id', $customerId)->delete();
    }
}
