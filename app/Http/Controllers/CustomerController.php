<?php

namespace App\Http\Controllers;

use App\Contracts\Services\Customer\CustomerServiceInterface;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Redirect;
use App\Http\Requests\ContactListRequest;
use App\Http\Requests\CustomerRequest;
use App\Http\Requests\EditCustomerRequest;
use App\Http\Requests\UpdateCustomerRequest;
use App\Http\Requests\UpdateRequest;
use Hash;
use Hybrid_Auth;
use Illuminate\Http\Request;
use Session;
use Validator;
use App\Models\Customer;

class CustomerController extends Controller
{
    private $customerService;

    public function __construct(CustomerServiceInterface $customer)
    {
        /* $this->customer = $customer;*/
        $this->customerService = $customer;
    }

    /*start of NLK*/
    public function showRegister()
    {
        session()->forget('customerid', 'customername', 'customeremail', 'pass', 'customerpassword', 'authority');
        return view('admin.user.user-regist');
    }

    public function register(Request $request)
    {

        $validator = Validator::make($request->all(),
            [
                'customername'     => 'required',
                'authority'        => 'required',
                'customerpassword' => 'required',
                'customeremail'    => 'required|email|unique:customer,email',
            ]
        );

        if ($validator->fails()) {
            return redirect(route('show-register'))
                ->withErrors($validator)
                ->withinput();
        }

        $customername     = $request->customername;
        $customeremail    = $request->customeremail;
        $customerpassword = $request->customerpassword;
        $authority        = $request->authority;

        $p    = "x";
        $pass = str_pad($p, strlen($customerpassword), "x");

        session(['customername' => $customername, 'customeremail' => $customeremail,
            'pass'                  => $pass, 'customerpassword'      => $customerpassword, 'authority' => $authority]);
        return view('admin.user.user-regist-confirm');
    }

    public function insert()
    {
        $customername     = session('customername');
        $customeremail    = session('customeremail');
        $customerpassword = Hash::make(session('customerpassword'));
        $authority        = session('authority');
        $customerid       = $this->customerService->autoId("customer", "customer_id", "OPE", 5);
        $id               = $this->customerService->insertRecord($customerid, $customername, $customeremail, $customerpassword, $authority);
        return view('admin.user.user-regist-result', ['id' => $id]);
    }

    public function editConfirm(UpdateCustomerRequest $request)
    {
        $customername     = $request->customername;
        $customeremail    = $request->customeremail;
        $customerpassword = $request->customerpassword;
        $authority        = $request->authority;
        if ($this->customerService->validateEmail($customeremail)) {
            $p    = "x";
            $pass = str_pad($p, strlen($customerpassword), "x");
            session(['customername' => $customername, 'customeremail' => $customeremail,
                'pass'                  => $pass, 'customerpassword'      => $customerpassword, 'authority' => $authority]);
            $customerid       = $this->customerService->autoId("customer", "customer_id", "OPE", 5);
            $customerpassword = Hash::make($request->customerpassword);
            $id               = $this->customerService->insertRecord($customerid, $customername, $customeremail, $customerpassword, $authority);
            return view('admin.user.user-regist-result', ['id' => $id]);
        } else {
            $error = 'Email exists! Choose another.';
            return view('admin.user.user-regist-edit', ['error' => $error]);
        }
    }

    public function showEditForm()
    {
        return view('admin.user.user-regist-edit');
    }

    public function showSearchForm()
    {
        return view('admin.user.user-search-form');
    }

    public function showLogin()
    {
        return view('admin.user.user-login-form');
    }

    public function contactList(ContactListRequest $request)
    {
        $id   = $request->id;
        $pass = $request->pass;
        $row  = $this->customerService->loginAction($id, $pass);
        if ($row == null) {
            $error = "User ID or Password does not match.";
            return view('/admin.user.user-login-form', ['error' => $error]);
        } else {
            $name = $row->customer_name;
            session(['login_name' => $name]);
            return view('admin.contact-list');
        }
    }

    public function logoutAction()
    {
        $gauth = new Hybrid_Auth(config_path() . '/google-auth.php');
        $gauth->logoutAllProviders();
        session()->forget('login_name');
        return redirect('/show-login');
    }
    /*end of NLK*/

    /*start of PMM*/

    public function index()
    {
        return view('index');
    }

    public function search(CustomerRequest $request)
    {
        $customerId   = $request->customerId;
        $customerName = $request->customer_name;
        $authority    = $request->authority;
        $email        = $request->email;
        $startDate    = $request->from;
        $endDate      = $request->to;
        session(['customer_id' => $customerId,
            'customer_name'        => $customerName,
            'authority'            => $authority,
            'email'                => $email,
            'startDate'            => $startDate,
            'endDate'              => $endDate,
        ]);
        $custLists = $this->customerService->search($customerId, $customerName, $authority, $email, $startDate, $endDate);
        return view('admin.user.search', [
            'custLists' => $custLists,
        ]);
    }

    public function edit(EditCustomerRequest $request)
    {
        $customerId = $request->customer_id;
        session(['customer_id' => $customerId]);
        $editList = $this->customerService->edit($customerId);
        return view('admin.user.edit', [
            'editList' => $editList,
        ]);
    }

    public function back()
    {
        $customerId   = session('customer_id');
        $customerName = session('customer_name');
        $authority    = session('authority');
        $email        = session('email');
        $startDate    = session('startDate');
        $endDate      = session('endDate');
        $custLists    = $this->customerService->search($customerId, $customerName, $authority, $email, $startDate, $endDate);

        return view('admin.user.search', [
            'custLists' => $custLists,
        ]);
    }

    public function update(UpdateRequest $request)
    {
        $customerId   = $request->customer_id;
        $customerName = $request->customer_name;
        $authority    = $request->authority;
        $email        = $request->email;
        session(['customer_id' => $customerId,
            'customer_name'        => $customerName,
            'authority'            => $authority,
            'email'                => $email,
        ]);
        $validator = Validator::make($request->all(), [
            'email' => 'unique:customer,email,' . $customerId . ',customer_id',
        ]);
        if ($validator->fails()) {
            $customerId = session('customer_id');
            $editList   = $this->customerService->edit($customerId);
            $validator  = $validator->messages();
            return view('admin.user.edit', [
                'editList' => $editList,
            ])->withErrors($validator);
        } else {
            return view('admin.user.update');
        }
    }

    public function editback()
    {
        $customerId = session('customer_id');
        $editList   = $this->customerService->edit($customerId);
        return view('admin.user.edit', [
            'editList' => $editList,
        ]);
    }

    public function store()
    {
        $customerId   = session('customer_id');
        $customerName = session('customer_name');
        $authority    = session('authority');
        $email        = session('email');
        $this->customerService->store($customerId, $customerName, $authority, $email);
        $customer_id = $customerId;
        Session::forget('customer_id', 'customer_name', 'authority', 'email');
        return view('admin.user.store', [
            'customer_id' => $customer_id,
        ]);
    }

    public function destroy()
    {
        $customerId = session('customer_id');
        $command    = $this->customerService->destroy($customerId);
        Session::forget('customer_name', 'authority', 'email');
        return view('admin/user/search-form');
    }

    public function ajaxSearch(Request $request)
    {
        $keywords = $request->keywords;
        $customers = Customer::all();

        foreach ($customers as $customer) {
            if(str_contains($customer->customer_name, $keywords))
            {
                $searchResults[] = $customer->customer_name;
            }
        }
        
        //var_dump($searchResults[0]["customer_name"]);die;
        return $searchResults;
    }

}
