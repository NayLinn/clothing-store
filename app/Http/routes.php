<?php
use App\Models\Customer;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
 */

Route::get('/', 'CustomerController@index');
Route::get('/show-login', 'CustomerController@showLogin');
Route::post('/contact-list', 'CustomerController@contactList');
Route::get('/logout-action', 'CustomerController@logoutAction');
Route::get('/gauth/{auth?}', array('as' => 'googleAuth', 'uses' => 'Auth\AuthController@getGoogleLogin'));
Route::get('/show-register', 'CustomerController@showRegister')->name('show-register');
Route::post('/register', 'CustomerController@register')->name('register');
Route::post('/insert', 'CustomerController@insert');
Route::post('/edit_confirm', 'CustomerController@editConfirm');
Route::get('/edit-form', 'CustomerController@showEditForm');

/*Route::post('/register', function(){
if(Request::ajax()){

var_dump(Response::json(Request::all()));
}
});*/

Route::get('/getRequest', function () {
    if (Request::ajax()) {
        return "Ajax has loaded completely. ";
    }
});

Route::get('/hqadmin', function () {
    return view('hq-admin/admin-layout');
});

Route::get('/hqadmin/customers', function () {
    $customers = Customer::all();
    return view('hq-admin/customers', [ 'customers' => $customers ]);
});

Route::get('/hqadmin/products', function () {
    $customers = Customer::all();
    return view('hq-admin/products',[ 'customers' => $customers ]);
});


Route::group(['middleware' => 'auth'], function () {

    Route::get('/home', 'CustomerController@index');

    Route::get('/products', function () {
        return view('/admin/contact-list');
    });

    Route::get('/admin/user/search-form', function () {
        return view('/admin/user/search-form');
    });

    Route::post('/admin/user/ajax-search', 'CustomerController@ajaxSearch');

    Route::get('/admin/user/search', 'CustomerController@search');

    Route::post('/admin/user/edit', 'CustomerController@edit');

    Route::get('/admin/user/search/back', 'CustomerController@back');

    Route::get('/admin/user/update', 'CustomerController@update');

    Route::get('/admin/user/search-form/back', 'CustomerController@destroy');

    Route::get('/admin/user/edit/back', 'CustomerController@editback');

    Route::post('/admin/user/store', 'CustomerController@store');

    Route::get('/admin/user/user-update', 'CustomerController@edit');

});
