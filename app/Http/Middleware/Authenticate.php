<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use Route;
use Illuminate\Support\Facades\Auth;

class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (!$request->session()->has('login_name')) {
            return redirect()->guest('show-login');
        }
        return $next($request);
    }
}
