<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Contracts\Dao\Customer\CustomerDaoInterface', 'App\Dao\Customer\CustomerDao');

        // Business logic registration
        $this->app->bind('App\Contracts\Services\Customer\CustomerServiceInterface', 'App\Services\Customer\CustomerService');
    }
}
