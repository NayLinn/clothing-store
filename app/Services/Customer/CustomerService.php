<?php

namespace App\Services\Customer;

use Hash;
use App\Contracts\Dao\Customer\CustomerDaoInterface;
use App\Contracts\Services\Customer\CustomerServiceInterface;

class CustomerService implements CustomerServiceInterface
{
    private $customerDao;

    public function __construct(CustomerDaoInterface $customerDao)
    {
        $this->customerDao = $customerDao;
    }

    /**
     * <h3>ログイン顧客情報取得</h3>
     * @param type $hcuMail
     * @return type
     */
    function validateEmail($customeremail){
        $customers = $this->customerDao->validateEmail($customeremail);

        if($customers==null){
            return true;
        }
        else{
            return false;
        }
    }

    function insertRecord($customerid,$customername,$customeremail,$customerpassword,$authority){
        $customerid=$this->customerDao->insertRecord($customerid,$customername,$customeremail,$customerpassword,$authority);
        return $customerid;
    }

    function autoId($tableName,$fieldName,$prefix,$noOfLeadingZeros){
        $row= $this->customerDao->autoId($tableName,$fieldName,$prefix,$noOfLeadingZeros);
        if ($row==null)
        {
            return $prefix . "00001";
        }
        else
        {
            $oldID=str_replace($prefix,"",$row);  //Removing "Prefix"
            $value=0;
            $value=(int)$oldID; //Convert to Integer
            $value++;
            $newID=$prefix.$this->NumberFormatter($value,$noOfLeadingZeros);
            return $newID;
        }
    }

    function NumberFormatter($number,$n)
    {
        return str_pad((int) $number,$n,"0",STR_PAD_LEFT);
    }

    function loginAction($id,$pass){
        $row= $this->customerDao->loginAction($id,$pass);
        foreach ($row as $key => $value) {
            if(Hash::check($pass,$value->password)){
                return $value;
            }
            else{
                return null;
            }
        }
    }

    ////////////////////////////////////////

    public function search($customerId, $customerName, $authority, $email, $startDate, $endDate)
    {
        if ($authority == "1") {
            $authority = 'wholesaler';
        } elseif ($authority == "2") {
            $authority = 'distributor';
        } elseif ($authority == "3") {
            $authority = 'retailer';
        } else {
            $authority = null;
        }

        return $this->customerDao->searchMenu($customerId, $customerName, $authority, $email, $startDate, $endDate);
    }

    public function edit($customerId)
    {
        return $this->customerDao->edit($customerId);
    }

    public function store($customerId, $customerName, $authority, $email)
    {
        return $this->customerDao->store($customerId, $customerName, $authority, $email);
    }

    public function destroy($customerId)
    {
        return $this->customerDao->destroy($customerId);
    }
}
