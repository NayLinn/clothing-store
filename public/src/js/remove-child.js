$(document).ready(function() {


    $("#cancel-by-id").click(function(e) {
        e.preventDefault();
        $(".cart-page").removeClass("initial");
        $(".cancel-box").css("display", "none");
    });

    $(".cart-items-list .item-qty input").bind('keyup change click', function(e) {
        var total = 0, allTotal = 0;
        var unit = "¥";

        var qty = $(this).closest(".cart-items-list li").find(".item-qty input").val();
        var unitPriceStr = $(this).closest(".cart-items-list li").find(".item-price").text();
        var unitPrice = unitPriceStr.substring(1, unitPriceStr.length);
        total = parseInt(qty) * parseFloat(unitPrice);
        $(this).closest(".cart-items-list li").find(".item-total").text(unit + total);

        $(".cart-items-list .item-total").each(function(e) {
            var str = $(this).text();
            var number = str.substring(1, str.length);
            allTotal += parseFloat(number);
        });
        $(".cart-result .item-total-price").text(unit + allTotal);

    });


    // Append Product variety
    $(".sub-product .add").click(function(e) {
        e.preventDefault();
        var table = $(".sub-product table");
        var row = $(".sub-product table tr").length;
        table.append(
            '<tr>
            <td class="operator">
            <a onclick="deleteRow(this)" class="delete">削除&nbsp;<i class="fa fa-trash-o"></i></a>
            </td>
            <td>
            <input type="text" name="varietyLists[productColor][]">
            </td>
            <td>
            <input type="text" name="varietyLists[productSize][]">
            </td>
            <td>
            <input type="text" for="price" name="varietyLists[unitPrice][]">
            </td>
            </tr>'
            );
        return false;
    });

    $(".sub-product .update-add").click(function(e) {
        e.preventDefault();
        var table = $(".sub-product table");
        var row = $(".sub-product table tr").length;
        table.append(
            '<tr>
            <td class="operator">
            <a onclick="deleteRow(this)" class="delete">削除&nbsp;<i class="fa fa-trash-o"></i></a>
            <input type="hidden" name="varietyLists[productVrietyId][]">
            </td>
            <td>
            <input type="text" name="varietyLists[productColor][]">
            </td>
            <td>
            <input type="text" name="varietyLists[productSize][]">
            </td>
            <td>
            <input type="text" for="price" name="varietyLists[unitPrice][]">
            </td>
            </tr>'
            );
        return false;
    });
});

// Remove Product variety
function deleteRow(row)
{
    var i=row.parentNode.parentNode.rowIndex;
    document.getElementById('ptable').deleteRow(i);
}




function doConfirm(pid) {
    var result = confirm("ID「" + pid +"」を削除しますか？");
    if(result) {
        document.getElementById("delete").href = "/product-delete/" + pid;
    } else {
        document.getElementById("delete").href = "/product-update-form/" + pid;
    }
}
