$.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$(document).ready(function() {

	var timer;
	$("#customer_name").on("keyup", up );
	$("#customer_name").on("keydown", down );

	function up() {
		$("#searchResults").empty();
		timer = setTimeout(function() {

			var keyword = $("#customer_name").val();
			var keywords = "keywords="+keyword;
			if(keywords.length > 0) {
				$.ajax({
		            type: "POST",
		            url: "ajax-search",
		            data: keywords,
		            success: function(data){
		            	$("#searchResults").empty();
		            	$.each(data, function (index, value) {
        					console.log(value);
        					$('<option >', {
							    value: value,
							}).appendTo("datalist");

        					/*$('#searchResults').append($('<p>').text(value));*/
        					
        					/*$.each(value, function (index, valuee) {
        						console.log(valuee); 
    						});*/
    					});

	            	}
	            });
			}

		},500);
	}

	function down() {
		clearTimeout(timer);
	}
});

