$(document).ready(function() {
    $( "#from-datepicker" ).datepicker(
      {
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        yearRange: "2010:2016"
      }
    );
    $( "#to-datepicker" ).datepicker(
      {
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        yearRange: "2010:2016"
      }
    );
});
