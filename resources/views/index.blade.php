@extends('layouts.layout')
@section('title','EC Site')

@section('content')
<!DOCTYPE html>
    <div class="content-wrapper">
      <div class="primary-content">
        <!-- End of Header -->
        <div class="login-page">

          <h3 class="page-header">This is index page.</h3>
          <div>
              <a href="gauth">Login using Google Plus</a>
          </div>
          <br>
          <div id="getRequest">
            <button type="button">Ajax</button>
          </div>
          <div id="getRequestData">

          </div>
          <br>
        </div>
      </div><!-- .primary-content -->
      <!-- Start of Footer -->
    </div><!-- .content-wrapper -->
    <footer class="footer-wrapper">
      <div class="copyright">
        <p> Copyright © SCM CO.,LTD All Rights Reserved.</p>
      </div><!-- .copyright -->
    </footer>
  </div><!-- .wrapper -->
@endsection('content')

@section('javascript')
  <script type="text/javascript">
    $(document).ready(function() {
      $("#getRequest").click(function(){
        $.get("getRequest", function(data){
          $("#getRequestData").append(data);
          console.log(data);
        });
      });
    });
  </script>
@endsection('javascript')
