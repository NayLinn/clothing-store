@extends('layouts.layout')
@section('title','EC Site')
@section('content')
  <!-- End of Header -->
        <h3 class="page-header">Update Data</h3>
        <div class="user-manage">
        <form action="{{ url('/admin/user/update') }}" method="get" class="user-frm">
            <div class="input-fields">
              <ul class="confirm-label remove-bullet">
                <li><span>CustomerID:</span></li>
                <li class="input-label"><input type="hidden" name="customer_id" value="{{ $editList->customer_id}}">{{ $editList->customer_id}}</li>
              </ul>

              <ul class="confirm-label remove-bullet">
                <li><span>Name:</span></li>
                <li class="input-label"><input type="text" class="input-field" name="customer_name" value="{{$editList->customer_name}}"/></li>
              </ul>

              <ul class="confirm-label remove-bullet">  
                <li><span>Authority:</span></li>
                <li class="input-label">
                  <select class="input-field" name="authority">
                    <option value="" selected="select"> Choose Your Authority </option>
                    <option value="wholesaler" @if($editList->authority == "wholesaler") {{ "selected" }} @endif  >Wholesaler</option>
                    <option value="distributor" @if($editList->authority == "distributor") {{ "selected" }} @endif >Distributor</option>
                    <option value="retailer" @if($editList->authority == "retailer") {{ "selected" }} @endif >Retailer</option>
                  </select>
                </li>
              </ul>

              <ul class="confirm-label remove-bullet">
                <li><span>Email:</span></li>
                <li class="input-label"><input type="email" class="input-field" name="email" value="{{$editList->email}}"/>
                </li>
              </ul>
            </div>
            <div class="submit-user">
              <a href="{{url('admin/user/search/back')}}" class="btnstyle" name="cancel">Cancel</a>
              <input type="submit" value="Update">
               {!! Form::open(array('url' => ['admin/user/search-form/back', $editList->customer_id], 'method' => 'get','enctype' => 'multipart/form-data')) !!}
                  <input type="button" name= "$editList->customer_id" class="delete" value="Delete" onclick="doConfirm('{{ $editList->customer_id}}')">
                {!! Form::close() !!}
            </form>
            </div>
        </div><!-- .user-manage -->

      <script type="text/javascript">

        function doConfirm(pid) {
          var result = confirm("ID「" + pid +"」を削除しますか？");
          if(result) {
          window.location = '/admin/user/search-form/back';
          }else{
            return redirect('admin/user/user-update');
          }
        }
      </script>

@endsection('content')