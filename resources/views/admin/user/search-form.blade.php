@extends('layouts.layout')
@section('title','EC Site')
@section('content')
<!-- End of Header -->
        <h3 class="page-header">Customer Search Form</h3>
        <div class="user-manage">
          <form action="{{url('admin/user/search')}}" class="user-frm">
            <div class="input-fields">
              <div class="search-frm">
                <ul>
                  <li>
                    <span>Authority:</span>
                  </li>
                  <li>
                    <select class="input-field" name="authority">
                      <option value="" selected="select">--- Choose Your Authority ---</option>
                      <option value="1">WholeSaler</option>
                      <option value="2">Distributor</option>
                      <option value="3">Retailer</option>
                    </select>
                  </li> 
                </ul>
              </div><!-- .search-frm -->

              <div class="search-frm">
                <ul>
                  <li>
                    <span>Customer Name:</span>
                  </li>
                  <li>
                    <input type="text" list="searchResults" name="customer_name" id="customer_name" >
                    <datalist id="searchResults">
                      <!-- <option value="Internet">
                      <option value="Firefox">
                      <option value="Chrome">
                      <option value="Opera">
                      <option value="Safari"> -->
                    </datalist>
                  </li>
                </ul>
              </div><!-- .search-frm -->
              <div class="search-frm">
                <ul>
                  <li>
                    <span>CustomerID:</span>
                  </li>
                  <li>
                    <input type="text" name="customerId">
                  </li>
                </ul>
              </div><!-- .search-frm -->
              <div class="search-frm">
                <ul>
                  <li>
                    <span>Email:</span>
                  </li>
                  <li>
                    <input type="email" name="email">
                  </li>
                </ul>
              </div><!-- .search-frm -->
              <div class="search-frm">
                <ul>
                  <li><span>Date:</span></li>
                  <li>
                    <input type="text" for="date" name="from" id="from-datepicker">
                    <span>~</span>
                    <input type="text" for="date" name="to" id="to-datepicker">
                  </li>
                </ul>
              </div><!-- .search-frm -->
            </div><!-- .input-fields -->
            <div class="submit-user search">
              <input type="submit" value="Search">
            </div>
          </form>
        </div><!-- .user-manage -->
@endsection('content')
