@extends('layouts.layout')
@section('title','EC Site')

@section('content')

<!-- End of Header -->
        <h3 class="page-header">Update Confirm</h3>
        <div class="user-manage">
          {!! Form::open(array('url' => 'admin/user/store',"class" => "user-frm")) !!}

            <div class="input-fields">
              <ul class="confirm-label remove-bullet">
                <li><span>CustomerID : </span></li>
                <li class="input-label">{{ session('customer_id') }}</li>
              </ul>
              
              <ul class="confirm-label remove-bullet">
                <li><span>Name : </span></li>
                <li class="input-label">{{session('customer_name') }}</li>
              </ul>
              
              <ul class="confirm-label remove-bullet">
                <li><span>Authority : </span></li>
                <li class="input-label">{{ session('authority') }}</li>
              </ul>
              
              <ul class="confirm-label remove-bullet">
                <li><span>Email : </span></li>
                <li class="input-label">{{ session('email')  }}</li>
              </ul>
            </div>

            <div class="submit-user">
              <a href="{{url('admin/user/edit/back')}}" class="btnstyle" name="back" value="戻る">戻る</a>
              <input type="submit" name="complete" value="更新する">
            </div>
            {!! Form::close() !!}
        </div>
@endsection('content')
