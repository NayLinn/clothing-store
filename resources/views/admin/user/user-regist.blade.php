@extends('layouts.layout')
@section('title','EC Site')

@section('content')

<!-- End of Header -->
        <h3 class="page-header">Customer Registration</h3>
        <div class="user-manage">
          <div class="user-manage">
            <form id="register" action="{{ url('/register') }}" class="user-frm" method="post">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <div class="input-fields">
                <ul>
                  <li><span>Customer Name:</span></li>
                  <li><input type="text" class="input-field" id="customername" name="customername" value="{{Input::get('customername')}}"/></li>
                  <li>@if ($errors->has('customername')) <p class="error_msg">{{ $errors->first('customername') }}</p> @endif</li>
                </ul>
                <ul>
                  <li><span>Authority:</span></li>
                  <li>
                    <select class="input-field" name="authority" id="authority">
                      <option value=" " selected="select">--- Choose Your Authority ---</option>
                      <option value="wholesaler"
                        @if(Input::get('authority') == "wholesaler")
                          {{"selected"}}
                        @endif>Wholesaler
                      </option>
                      <option value="distributor"
                        @if(Input::get('authority') == "distributor")
                          {{"selected"}}
                        @endif>
                        Distributor
                        </option>
                      <option value="retailer"
                        @if(Input::get('authority') == "retailer")
                          {{"selected"}}
                        @endif>
                        Retailer
                        </option>
                    </select>
                  </li>
                  <li>@if ($errors->has('authority')) <p class="error_msg">{{ $errors->first('authority') }}</p> @endif</li>
                </ul>
                <ul>
                  <li><span>Password:</span></li>
                  <li><input type="Password" class="Password" name="customerpassword" value="" id="customerpassword" /></li>
                  <li>@if ($errors->has('customerpassword')) <p class="error_msg">{{ $errors->first('customerpassword') }}</p> @endif</li>
                </ul>
                <ul>
                  <li><span>Confirm Password:</span></li>
                  <li><input type="password" class="password" name="customerpassword_confirmation" id="customerpassword_confirmation"  /></li>
                  <li>@if ($errors->has('customerpassword')) <p class="error_msg">{{ $errors->first('customerpassword') }}</p> @endif</li>
                </ul>
                <ul>
                  <li><span>Email:</span></li>
                  <li><input type="text" class="input-field" id="customeremail" name="customeremail" value="{{Input::get('customeremail')}}" /></li>
                  <li>@if(isset($error))<p class="error_msg">{{$error}}</p>@endif
                  @if ($errors->has('customeremail')) <p class="error_msg">{{ $errors->first('customeremail') }}</p> @endif</li>
                </ul>
              </div><!-- .input-fields -->

              <div class="submit-user">
                <input type="submit" value="register"/>
              </div>
            </form>
          </div><!-- .user-manage -->
        </div><!-- .user-manage -->

@endsection('content')

@section('javascript')
  
  <div id="postRequestData"></div>
    <script type="text/javascript">
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });

      $(document).ready(function(){
        $("#register").submit(function(){
          var customername= $("#customername").val();
          var authority = $("#authority").val();
          var customeremail=$("#customeremail").val();
          var customerpassword=$("#customerpassword").val();
          var customerpassword_confirmation=$("#customerpassword_confirmation").val();

          /*$.post("register", { customerName:customerName, authority:authority}, function(data){
            console.log(data);
            $("#postRequestData").html(data);
          });*/

          var dataString = "customername="+customername+"&authority="+authority+"&customeremail="+customeremail+"&customerpassword="+customerpassword+"&customerpassword_confirmation="+customerpassword_confirmation;
          $.ajax({
            type: "POST",
            url: "register",
            data: dataString,
            success: function(data){
              $(".wrapper").hide();
              console.log(data);
              $("#postRequestData").html(data);
            }
          });


        });
      });
    </script>

@endsection('javascript')