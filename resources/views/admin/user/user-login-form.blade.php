@extends('layouts.layout')
@section('title','EC Site')

@section('content')
<!DOCTYPE html>
    <div class="content-wrapper">
      <div class="primary-content">
        <!-- End of Header -->
        <div class="login-page">

          <h3 class="page-header">Login Form</h3>
          <form action="{{ url('/contact-list') }}" method="post" class="login-form">
          {{ csrf_field() }}
            <div>
              <span>User_ID<label class="require"> *</label></span>
              <input type="text" name="id">
            </div>
            <div>
              <span>Password<label class="require"> *</label></span>
              <input type="password" name="pass">
            </div>
            @if(isset($error))<span><label class="error">{{$error}}</label></span>@endif
            <div>
              <input value="Login" type="submit">
            </div>
            <div>
              <a href="gauth">Login using Google Plus</a>
            </div>
          </form>

        </div>
      </div><!-- .primary-content -->
      <!-- Start of Footer -->
    </div><!-- .content-wrapper -->
    <footer class="footer-wrapper">
      <div class="copyright">
        <p> Copyright © SCM CO.,LTD All Rights Reserved.</p>
      </div><!-- .copyright -->
    </footer>
  </div><!-- .wrapper -->
@endsection('content')