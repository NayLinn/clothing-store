@extends('layouts.layout')
@section('title','EC Site')

@section('content')

<!-- End of Header -->
        <h3 class="page-header">Please Update Data to Register in Database.</h3>
        <div class="user-manage">
          <form action="{{ url('/edit_confirm') }}" name="myform" method="post" class="user-frm">
            {{ csrf_field() }}
            <div class="input-fields">
              <ul>
                <li><span>Name:</span></li>
                <li><input type="text" class="input-field" name="customername" value="@if(session('login_name')){{ session('login_name')}}
                @else{{ session('customername')}} @endif"/></li>
                <li>@if ($errors->has('customername')) <p class="error_msg">{{ $errors->first('customername') }}</p> @endif</li>
              </ul>
              <ul>
                <li><span>Authority:</span></li>
                <li>
                  <select class="input-field" name="authority" value="">
                    <option value=" ">--- Choose Your Authority ---</option>
                    <option value="wholesaler" @if(session('authority') == "wholesaler"){{"selected"}}

                    @endif>Wholesaler</option>
                    <option value="distributor" @if(session('authority') == "distributor"){{"selected"}}

                    @endif>Distributor</option>
                    <option value="retailer" @if(session('authority') == "retailer"){{"selected"}}

                    @endif>Retailer</option>
                  </select>
                </li>
                <li>@if ($errors->has('authority')) <p class="error_msg">{{ $errors->first('authority') }}</p> @endif</li>
              </ul>
              <ul>
                <li><span>Password:</span></li>
                <li><input type="password" class="Password" name="customerpassword" value="{{ session('customerpassword')}}" /></li>
                <li>@if ($errors->has('customerpassword')) <p class="error_msg">{{ $errors->first('customerpassword') }}</p> @endif</li>
              </ul>
              <ul>
                <li><span>Email:</span></li>
                <li><input type="text" class="input-field" name="customeremail" value="{{ session('customeremail')}}" /></li>
                <li>
                  @if ($errors->has('customeremail')) <p class="error_msg">{{ $errors->first('customeremail') }}</p> @endif
                  @if(isset($error))<p class="error_msg">{{$error}}</p>
                  @endif
                </li>
              </ul>
            </div><!-- .input-fields -->

            <div class="submit-user">
              <input type="submit" name="cancle" value="Confirm">
            </div>
          </form>
        </div><!-- .user-manage -->

@endsection('content')
