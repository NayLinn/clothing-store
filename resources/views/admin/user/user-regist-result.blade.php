@extends('layouts.layout')
@section('title','EC Site')

@section('content')
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<!-- End of Header -->
        <h3 class="page-header">Information successfully saved.</h3>
        <div class="user-manage">
          <form action="{{ url('/show-register') }}" name="myform" method="get" class="user-frm">
            {{ csrf_field() }}
            <div class="input-fields">
              <!-- <ul class="confirm-label">
                <li><span>CustomerID:</span></li>
                <li class="input-label">OPE00001</li>
              </ul> -->
              <ul class="confirm-label">
                <li><span>ID:</span></li>
                <li class="input-label">{{ $id }}</li>
              </ul>
              <ul class="confirm-label">
                <li><span>Name:</span></li>
                <li class="input-label">{{ session('customername')}}</li>
              </ul>
              <ul class="confirm-label">
                <li><span>Authority:</span></li>
                <li class="input-label">{{ session('authority')}}</li>
              </ul>
              <ul class="confirm-label">
                <li><span>Password:</span></li>
                <li class="input-label">{{ session('pass')}}</li>
              </ul>
              <ul class="confirm-label">
                <li><span>Email:</span></li>
                <li class="input-label">{{ session('customeremail')}}</li>
              </ul>
            </div><!-- .input-fields -->

            <div class="submit-user">
              <input style="float: left;" type="submit" value="OK"/>
            </div>
          </form>
        </div><!-- .user-manage -->

@endsection('content')
