@extends('layouts.layout')
@section('title','EC Site')

@section('content')
        <h3 class="page-header">Customer Search</h3>
        <div class="user-manage">
          <form action="{{url('admin/user/search')}}" class="user-frm" >
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="input-fields">
              <div class="search-frm">
                <ul>
                  <li>
                    <span>Authority:</span>
                  </li>
                  <li>
                    <select class="input-field" name="authority">
                      <option value="" selected="select">--- Choose Your Authority ---</option>
                      <option value="1">Wholesaler</option>
                      <option value="2">Distributor</option>
                      <option value="3">Retailer</option>
                    </select>
                  </li>
                </ul>
              </div><!-- .search-frm -->
              <div class="search-frm">
                <ul>
                  <li>
                    <span>Customer Name:</span>
                  </li>
                  <li>
                    <input type="text" list="searchResults" name="customer_name" id="customer_name" >
                    <datalist id="searchResults">
                      <!-- <option value="Internet">
                      <option value="Firefox">
                      <option value="Chrome">
                      <option value="Opera">
                      <option value="Safari"> -->
                    </datalist>
                  </li>
                </ul>
                <!-- <ul>
                  <li id="searchResults">
                  
                  </li>
                </ul> -->
              </div><!-- .search-frm -->
              <div class="search-frm">
                <ul>
                  <li>
                    <span>CustomerID:</span>
                  </li>
                  <li>
                    <input type="text" name="customerId" value="{{Input::get('customerId')}}">
                  </li>
                </ul>
              </div><!-- .search-frm -->
              <div class="search-frm">
                <ul>
                  <li>
                    <span>Email:</span>
                  </li>
                  <li>
                    <input type="text" name="email" value="{{Input::get('email')}}">
                  </li>
                </ul>
              </div><!-- .search-frm -->
              <div class="search-frm">
                <ul>
                  <li><span>Date:</span></li>
                  <li>
                    <input type="text" for="date" name="from" id="from-datepicker" value="{{Input::get('from')}}" >
                    <span>~</span>
                    <input type="text" for="date" name="to" id="to-datepicker" value="{{Input::get('to')}}" >
                  </li>
                </ul>
              </div><!-- .search-frm -->
            </div><!-- .input-fields -->
            <div class="submit-user search">
              <input type="submit" value="Search">
            </div>
          </form>
        @if(sizeof($custLists) > 0)
          <p style="color:green;margin-left:30px;">Great! Here is your result.Thanks;</p>
          <div class="user-table">
            <table class="search-result">
              <thead>
                <tr>
                  <th class="operator"></th>
                  <th class="id">CustomerID</th>
                  <th>Name</th>
                  <th class="authority">Authority</th>
                  <th>Email</th>
                  <th class="date">
                    Created Date
                  </th>
                  <th class="date">
                    Updated Date
                  </th>
                </tr>
              </thead>
              <tbody>
              @foreach($custLists as $custList)
                <tr>
                <td class="operator">
                    {!! Form::open(array('url' => 'admin/user/edit')) !!}
                        {!! Form::hidden('customer_id', $custList->customer_id) !!}

                        {!! Form::submit('edit', $attributes = ['class' => 'icon-edit']); !!}
                    {!! Form::close() !!}
                  </td>
                  <td class="id"> {{$custList->customer_id}}</td>
                  <td> {{$custList->customer_name}}</td>
                  <td class="authority"> {{$custList->authority}}</td>
                  <td> {{$custList->email}}</td>
                  <td class="date"> {{date('Y-m-d', strtotime($custList->register_datetime))}}</td>
                  <td class="date"> {{date('Y-m-d', strtotime($custList->update_datetime))}}</td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
      </div>
      <div class="paginate">
        <ul class="pagination nav">
            @if(Input::get('page') <= $custLists->lastPage())
                  {{
                    $custLists->appends(['customer_id' =>Input::get('customer_id'),'customer_name' =>Input::get('customer_name'),'email' =>Input::get('email'),'authority' => Input::get('authority'),'from' =>Input::get('from'),'to' =>Input::get('to') ])->links()
                  }}
            @endif
        </ul>
      </div>
          @else
              <p style="color:red;margin-left:30px;">Please choose one.Try again Later</p>
          @endif
        </div>
@endsection('content')
