@extends('layouts.layout')
@section('title','EC Site')
@section('content')
<!-- End of Header -->
        <h3 class="page-header">Update Complete</h3>
        <div class="user-manage">
          <p class="complete">
            CustomerID {{ $customer_id}} is updated.
          </p>
          <form action="{{url('admin/user/search')}}" class="user-frm">
            <div class="input-fields">
              <div class="search-frm">
                <ul>
                  <li>
                    <span>Authority:</span>
                  </li>
                  <li>
                    <select class="input-field" name="authority">
                      <option value="" selected="select">--- Choose Your Authority ---</option>
                      <option value="wholesaler">Wholesaler</option>
                      <option value="distributor">Distributor</option>
                      <option value="retailer">Retailer</option>
                    </select>
                  </li>
                </ul>
              </div><!-- .search-frm -->
              <div class="search-frm">
                <ul>
                  <li>
                    <span>Customer Name:</span>
                  </li>
                  <li>
                    <input type="text" name="customer_name" value="">
                  </li>
                </ul>
              </div><!-- .search-frm -->
              <div class="search-frm">
                <ul>
                  <li>
                    <span>CustomerID:</span>
                  </li>
                  <li>
                    <input type="text" name="customerId">
                  </li>
                </ul>
              </div><!-- .search-frm -->
              <div class="search-frm">
                <ul>
                  <li>
                    <span>Email:</span>
                  </li>
                  <li>
                    <input type="email" name="email">
                  </li>
                </ul>
              </div><!-- .search-frm -->
              <div class="search-frm">
                <ul>
                  <li><span>Date:</span></li>
                  <li>
                    <input type="text" for="date" name="from" id="from-datepicker">
                    <span>~</span>
                    <input type="text" for="date" name="to" id="to-datepicker">
                  </li>
                </ul>
              </div><!-- .search-frm -->
            </div><!-- .input-fields -->
            <div class="submit-user search">
              <input type="submit" value="Search">
            </div>
          </form>
        </div><!-- .user-manage -->

@endsection('content')
