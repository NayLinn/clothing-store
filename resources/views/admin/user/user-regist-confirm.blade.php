@extends('layouts.layout')
@section('title','EC Site')

@section('content')
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<!-- End of Header -->
        <h3 class="page-header">Register Confirmation</h3>
        <div class="user-manage">
          <form action="{{ url('/insert') }}" name="myform" method="post" class="user-frm">
            {{ csrf_field() }}
            <div class="input-fields">
              <ul class="confirm-label">
                <li><span>Name:</span></li>
                <li class="input-label">{{ session('customername')}}</li>
              </ul>
              <ul class="confirm-label">
                <li><span>Authority:</span></li>
                <li class="input-label">{{ session('authority')}}</li>
              </ul>
              <ul class="confirm-label">
                <li><span>Password:</span></li>
                <li class="input-label">{{ session('pass')}}</li>
              </ul>
              <ul class="confirm-label">
                <li><span>Email:</span></li>
                <li class="input-label">{{ session('customeremail')}}</li>
              </ul>
            </div><!-- .input-fields -->

            <div class="submit-user">
              <input type="submit" value="Confirm" class="btnstyle"/>
              <a href="{{ url('/edit-form')}}" class="btnstyle">
                  Edit
              </a>
            </div>
          </form>
        </div><!-- .user-manage -->

@endsection('content')
