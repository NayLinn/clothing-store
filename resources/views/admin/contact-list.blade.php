@extends('layouts.layout')
@section('title','EC Site')

@section('content')

<!-- End of Header -->
        <h3 class="page-header">
          Contact List
        </h3>
        <div class="contact-reply">
          <div class="contact-reply-table">
            <table class="reply-table">
              <thead>
                <tr>
                  <th class="operator"></th>
                  <th class="contact-id">CustomerID</th>
                  <th class="contact-name">Name</th>
                  <th class="contact-mail">Email</th>
                  <th class="contact-detail">Detail</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td class="operator">
                    <a href="contact/contact-update.html" class="edit">
                      編集&nbsp;
                      <i class="icon-edit"></i>
                    </a>
                  </td>
                  <td class="contact-id">CON20161206105305100</td>
                  <td class="contact-name">Jackie Chesterfield</td>
                  <td class="contact-mail">swill@weigelite.edu</td>
                  <td class="contact-detail">
                    <p>
                      tipple ressaldar sweatiness princess creeping ageusia frontierman hucksterage traducible frownful equiglacial scrupular euphonical toxophil lighting cocreator repaganize katathermometer centronucleus unmenacing Serpentarian analyze pluperfectness gnatworm
                    </p>
                  </td>
                </tr>
                <tr>
                  <td class="operator">
                    <a href="contact/contact-update.html" class="edit">
                      編集&nbsp;
                      <i class="icon-edit"></i>
                    </a>
                  </td>
                  <td class="contact-id">CON20161206105305101</td>
                  <td class="contact-name">Tonette Narasimhan</td>
                  <td class="contact-mail">gangliate@whitsun.net</td>
                  <td class="contact-detail">
                    <p>
                      nea bland Newmanize obeah unoverflowing incultivation phenomenalize rathe clathrate Malleifera scapuloulnar outstunt hypidiomorphic dishwater rimer agromyzid fiorin Cassiopeia osphyitis epaxial Samaritaness aureateness necronite topiarian
                    </p>
                  </td>
                </tr>
                <tr>
                  <td class="operator">
                    <a href="contact/contact-update.html" class="edit">
                      編集&nbsp;
                      <i class="icon-edit"></i>
                    </a>
                  </td>
                  <td class="contact-id">CON20161206105305102</td>
                  <td class="contact-name">Margot Braboy</td>
                  <td class="contact-mail">thrinax@lopper.co.uk</td>
                  <td class="contact-detail">
                    <p>
                      stumper fancy durn supertranscendent heresiologist isopyre tortious bewreck headrope tylotate coadunatively duyker lardizabalaceous flabbiness detachability bench cysted serine nagman Popocracy asleep oysterroot amusedly riddler
                    </p>
                  </td>
                </tr>
                <tr>
                  <td class="operator">
                    <a href="contact/contact-update.html" class="edit">
                      編集&nbsp;
                      <i class="icon-edit"></i>
                    </a>
                  </td>
                  <td class="contact-id">CON20161206105305103</td>
                  <td class="contact-name">Noemi Unruh</td>
                  <td class="contact-mail">autographical@modiolus.org</td>
                  <td class="contact-detail">
                    <p>
                      jukebox exopodite vage dollishly oscillatorian precipitancy covillager epileptic coscoroba metra slowheaded partigen Wiltshire lionlike superenforcement reclama unenshrined aldermanlike exhortation jawless yammadji twaddle allophyle hydatid
                    </p>
                  </td>
                </tr>
                <tr>
                  <td class="operator">
                    <a href="contact/contact-update.html" class="edit">
                      編集&nbsp;
                      <i class="icon-edit"></i>
                    </a>
                  </td>
                  <td class="contact-id">CON20161206105305104</td>
                  <td class="contact-name">Latrice Debar</td>
                  <td class="contact-mail">sailed@nonfederal.org</td>
                  <td class="contact-detail">
                    <p>
                      aleuritic discrepant beslushed moreness tiriba arrhenotoky unparticular Cashmere karyomere billsticking ironware pseudomorphia dynamotor antenatal unvivid squaredly itamalate arcticwards endosiphon exorcise knockoff Threskiornithidae autoinhibited nondepreciating
                    </p>
                  </td>
                </tr>
                <tr>
                  <td class="operator">
                    <a href="contact/contact-update.html" class="edit">
                      編集&nbsp;
                      <i class="icon-edit"></i>
                    </a>
                  </td>
                  <td class="contact-id">CON20161206105305105</td>
                  <td class="contact-name">Terrie Woerner</td>
                  <td class="contact-mail">divertisement@rheostatic.org</td>
                  <td class="contact-detail">
                    <p>
                      concertgoer prodeportation unallayable congested trionychoid cordigeri intertwist subaquatic stymie unjuiced nid superpetrosal ped sapphic waitingly peristrephic smirky intexture Zapara frankpledge ununderstandably undodged Hainai trilateralness
                    </p>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div><!-- .contact-manage -->

@endsection('content')
