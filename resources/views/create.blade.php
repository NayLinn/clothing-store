@extends('layouts.layout')
@section('title','EC Site')

@section('content')

<!-- End of Header -->
        <h3 class="page-header">Customer Registration</h3>
        <div class="user-manage">
          <div class="user-manage">
            <form action="user-regist-confirm.html" name="myform" method="" class="user-frm">
              <div class="input-fields">
                <!-- <ul>
                  <li><span>作業者ID:</span></li>
                  <li><input type="text" class="input-field" name="operator_id" value="" /></li>
                </ul> -->
                <ul>
                  <li><span>Customer Name:</span></li>
                  <li><input type="text" class="input-field" name="name"/></li>
                </ul>
                <ul>
                  <li><span>Authority:</span></li>
                  <li>
                    <select class="input-field" name="authority">
                      <option value="" selected="select">--- Choose Your Authority ---</option>
                      <option value="Wholesaler">Wholesaler</option>
                      <option value="Distributor">Distributor</option>
                      <option value="Retailer">Retailer</option>
                    </select>
                  </li>
                </ul>
                <ul>
                  <li><span>Password:</span></li>
                  <li><input type="Password" class="Password" name="password" value="" /></li>
                </ul>
                <ul>
                  <li><span>Email:</span></li>
                  <li><input type="text" class="input-field" name="email" value="" /></li>
                </ul>
              </div><!-- .input-fields -->

              <div class="submit-user">
                <input type="submit" value="Register"/>
              </div>
            </form>
          </div><!-- .user-manage -->
        </div><!-- .user-manage -->

@endsection('content')
