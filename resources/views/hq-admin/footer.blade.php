<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        Version 2.4.0
    </div>
    <!-- Default to the left -->
    <strong>Copyright © 2018 <a href="#">Lupine Co.</a></strong> All rights reserved.
</footer>