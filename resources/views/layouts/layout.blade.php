<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <title>@yield('title')</title>
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/admin.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/style.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/min/admin.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/min/style.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('src/font-awesome/css/font-awesome-icon.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('src/font-awesome/css/font-awesome-unicode.min.css') }}">
  <meta name="csrf-token" content="{{ csrf_token() }}" />
</head>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/59ca1357c28eca75e46224eb/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
<body>
  <div class="wrapper">
    <header class="header-wrapper">
      <div class="admin-header">
        <div class="site-logo">
          <a href="{{ url('/home') }}"><img src="{{ URL::asset('src/img/admin-logo-header.png') }}" alt="EC Site Logo"></a>
        </div><!-- .site-logo -->
        <div class="admin-menu">
          <ul class="nav">
            <li>
              <a href="#">@if(session('login_name')){{session('login_name')}}@endif</a>
              <ul>
                <li><a href="{{ url('/reset') }}" class="setting">Reset</a></li>
                <li>@if(session('login_name'))<a href="{{ url('/logout-action') }}" class="signout">{{"Logout"}}
                @else<a href="{{ url('/show-login') }}" class="signout">{{"Login"}}
                @endif</a></li>
              </ul>
            </li>
          </ul>
        </div>
      </div><!-- .site-header -->
      <div class="main-menu">
        <ul class="nav">
          <li class="active">
            <a href="{{ url('/products') }}">Product List</a>
          </li>
          <li class="active">
            <a href="#">Customer List</a>
            <ul>
              <li>
                <a href="{{ url('/show-register') }}">Register</a>
              </li>
              <li>
                <a href="{{ url('/admin/user/search-form') }}">Search</a>
              </li>
            </ul>
          </li>
        </ul>
      </div><!-- .sidebar -->
    </header><!-- .header-wrapper -->

    <div class="content-wrapper">
      <div class="primary-content">
        <!-- End of Header -->
          @yield('content')

        <!-- Start of Footer -->
      </div><!-- .primary-content -->
    </div><!-- .content-wrapper -->
    <footer class="footer-wrapper">
      <div class="copyright">
        <p> Copyright © SCM CO.,LTD All Rights Reserved.</p>
      </div><!-- .copyright -->
    </footer>
  </div><!-- .wrapper -->

  <script src="{{ URL::asset('src/js/bundle.js') }}"></script>
  <script src="{{ URL::asset('src/js/jquery-1.7.2.js') }}"></script>
  <script src="{{ URL::asset('src/js/jquery.bxslider.min.js') }}"></script>
  <script src="{{ URL::asset('src/js/remove-child.js') }}"></script>
  <script src="{{ URL::asset('src/js/toggle.js') }}"></script>
  <script src="{{ URL::asset('src/js/ajax-search.js') }}"></script>

  <script type="text/javascript" src="{{ URL::asset('src/datepicker/date-picker.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('src/datepicker/jquery-1.10.2.min.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('src/datepicker/jquery-ui.js') }}"></script>
  @yield('javascript')
</body>
</html>
